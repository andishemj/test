package com.example.ankotest.adapter

/**
 * @author mastermj94@gmail.com (mohammad jabbari)
 * adapter for food recycler view
 * */


import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ankotest.model.FoodModel
import com.example.ankotest.ui.main.RcyUI
import com.squareup.picasso.Picasso
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.imageBitmap

class FoodAdapter (var list: List<FoodModel>) : RecyclerView.Adapter<FoodAdapter.FoodViewHolder>() {
override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodViewHolder {
    return FoodViewHolder(RcyUI().createView(AnkoContext.create(parent.context, parent)))
}

override fun onBindViewHolder(holder: FoodViewHolder, position: Int) {
    holder.tvTitle.text = list[position].title
    holder.tvDesc.text = list[position].subTitle
    Picasso.get().load(list[position].image).into(holder.imgThemp)
    holder.tvRate.text = "${list[position].rate}"
}

override fun getItemCount(): Int {
    return list.size
}

inner class FoodViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var tvTitle: TextView
    var tvDesc : TextView
    var  tvTime : TextView
    var tvRate : TextView
    var imgThemp : ImageView

    init {
        tvTitle = itemView.findViewById(RcyUI.tvTitleId)
        tvDesc = itemView.findViewById(RcyUI.tvDescId)
        tvTime = itemView.findViewById(RcyUI.tvTimeId)
        tvRate = itemView.findViewById(RcyUI.tvRateId)
        imgThemp = itemView.findViewById(RcyUI.imgThemp)
    }

}
}