package com.example.ankotest.model

data class FoodModel(
    val id: String,
    val title: String,
    val subTitle: String,
    val description: String,
    val ingredients: String,
    val image: String,
    val gallery: String,
    val price: Double,
    val fee: Int,
    val rate: Int,
    val isAcceptingDelivery: Boolean,
    val isAcceptingPickup: Boolean,
    val isFavorite: Boolean,
    val isCatering: Boolean,
    val isAvailable: Boolean,
    val cuisineType: CuisineType,
    val mealType: MealType,
    val menuType: MenuType,
    val courseType: CourseType,
    val special: Special,
    val rewards: List<Any>,
    val delivery: Int,
    val maximumCalorie: Int,
    val minimumCalorie: Int,
    val preparation: Int
)