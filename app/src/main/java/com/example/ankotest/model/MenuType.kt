package com.example.ankotest.model

data class MenuType(
    val title: String,
    val description: String,
    val image: String,
    val priority: Int
)