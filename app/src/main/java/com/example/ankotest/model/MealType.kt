package com.example.ankotest.model

data class MealType(
    val title: String,
    val description: String,
    val image: String,
    val priority: Int
)