package com.afra.reformpowermeter.di.module

/**
 * @author mastermj94@gmail.com (mohammad jabbari)
 * view model di
 * */
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.afra.reformpowermeter.Base.ViewModelFactory
import com.example.ankotest.di.scope.ViewModelKey
import com.example.ankotest.ui.main.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory


    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    open abstract fun providerMainViewModel(mainViewModel: MainViewModel): ViewModel

}