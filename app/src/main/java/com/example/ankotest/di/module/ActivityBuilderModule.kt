package com.afra.reformpowermeter.di.module

/**
 * @author mastermj94@gmail.com (mohammad jabbari)
 * will provide activities whit all module
 * */

import com.example.ankotest.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {


    @ContributesAndroidInjector(modules = arrayOf())
    abstract fun provideMainActivity():MainActivity

}