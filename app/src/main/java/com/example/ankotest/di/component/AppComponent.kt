package com.example.ankotest.di.component

import android.app.Application
import com.afra.reformpowermeter.di.module.ActivityBuilderModule
import com.example.ankotest.Application.AnkoApplication
import com.example.ankotest.di.module.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * @author mastermj94@gmail.com (mohammad jabbari)
 * main component that handel all di and inject in PowerMeterApplication
 * */

@Singleton
@Component(
    modules = [AndroidSupportInjectionModule::class, AppModule::class, ActivityBuilderModule::class]
)
interface AppComponent : AndroidInjector<DaggerApplication> {

    fun inject(ankoApplication: AnkoApplication)


    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        //get context of app for app module and its use for all di that need context
        fun appModule(appModule: AppModule): Builder


        fun build(): AppComponent
    }

}