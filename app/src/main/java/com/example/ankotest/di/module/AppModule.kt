package com.example.ankotest.di.module
/**
 * @author mastermj94@gmail.com (mohammad jabbari)
 * app module that provide base app di like context
 * */
import android.content.Context
import com.afra.reformpowermeter.di.module.ViewModelModule
import com.example.ankotest.Application.AnkoApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = arrayOf(ViewModelModule::class))
class AppModule(val context : AnkoApplication)
{

    @Singleton
    @Provides
    fun providesContext() : Context
    {
        return context
    }
}