package com.example.ankotest.ui.main

import android.os.Bundle
import com.afra.reformpowermeter.Base.BaseActivity
import com.example.ankotest.adapter.FoodAdapter
import com.example.ankotest.adapter.FoodSlideShowAdapter
import org.jetbrains.anko.setContentView
import javax.inject.Inject


class MainActivity : BaseActivity() {
    @Inject
    lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainUI(FoodAdapter(mainViewModel.convertList()!!), FoodSlideShowAdapter(mainViewModel.convertList()!!)).setContentView(this)
    }
}
