package com.example.ankotest.ui.main

/**
 * @author mastermj94@gmail.com (mohammad jabbari)
 * logic part of food view
 * */

import android.content.Context
import com.afra.reformpowermeter.Base.BaseViewModel
import com.example.ankotest.model.*
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

import timber.log.Timber
import java.io.IOException
import java.lang.reflect.Type
import javax.inject.Inject



class MainViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var context: Context


    //get data ready from json file
    fun convertList(): List<FoodModel>? {

        val type = object : TypeToken<List<FoodModel>>() {}.type
        val result: List<FoodModel> = parseArray<List<FoodModel>>(json = getAssetJsonData().toString(), typeToken = type)

        return result
    }


    fun getAssetJsonData(): String? {
        val json: String
        try {
            val inputStream = context.getAssets().open("menu.json")
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.use { it.read(buffer) }
            json = String(buffer)
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        // print the data
        Timber.i("data $json")
        return json

    }
    inline fun <reified T> parseArray(json: String, typeToken: Type): T {
        val gson = GsonBuilder().create()
        return gson.fromJson<T>(json, typeToken)
    }

}