package com.example.ankotest.ui.main

import android.view.View
import androidx.core.view.marginBottom
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ankotest.adapter.FoodAdapter
import com.example.ankotest.adapter.FoodSlideShowAdapter
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

class MainUI(val listAdapter: FoodAdapter, val slideAdapter : FoodSlideShowAdapter): AnkoComponent<MainActivity> {
    override fun createView(ui: AnkoContext<MainActivity>): View = with(ui){
        return verticalLayout{
            padding = dip(16)
            lparams (width = matchParent, height = wrapContent)

            recyclerView{
                lparams (width = matchParent, height = wrapContent)
                layoutManager = LinearLayoutManager(ctx, LinearLayoutManager.HORIZONTAL, false)
                adapter = slideAdapter

            }.lparams{
                bottomMargin = dip(16)
            }

            recyclerView{
                lparams (width = matchParent, height = wrapContent)
                layoutManager = LinearLayoutManager(ctx)
                adapter = listAdapter
            }
        }
    }
}
