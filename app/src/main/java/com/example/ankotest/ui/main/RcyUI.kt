package com.example.ankotest.ui.main

import android.annotation.TargetApi
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.example.ankotest.R
import org.jetbrains.anko.*
import org.jetbrains.anko.custom.style


class RcyUI : AnkoComponent<ViewGroup> {

    companion object {
        val tvTitleId = 1
        val tvDescId = 2
        val tvTimeId = 3
        val tvRateId = 4
        val imgThemp = 5
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
        linearLayout {
            background = resources.getDrawable(R.drawable.shadow_box)
            lparams {
                margin = 16
                width = matchParent
                height = 300

            }

            imageView {
                id = imgThemp
                layoutParams = LinearLayout.LayoutParams(250, matchParent)
                setImageDrawable(resources.getDrawable(R.drawable.not_loaded))
                scaleType = ImageView.ScaleType.CENTER_CROP


            }

            verticalLayout {
                lparams(matchParent, wrapContent)
                padding = dip(10)
                textView {
                    id = tvTitleId
                    layoutParams = LinearLayout.LayoutParams(matchParent, wrapContent)
                    text = "Sherlock"
                    textSize = 16f // <- it is sp, no worries
                    textColor = Color.BLUE
                    typeface = Typeface.DEFAULT_BOLD

                }
                textView {
                    id = tvDescId
                    layoutParams = LinearLayout.LayoutParams(matchParent, wrapContent)
                    text = "2009"
                    textSize = 14f
                    textColor = Color.GRAY

                }

                linearLayout {

                    textView {
                        id = tvTimeId
                        layoutParams = LinearLayout.LayoutParams(wrapContent, wrapContent)
                        gravity = bottom
                        textColor = Color.BLACK

                    }

                    verticalLayout {
                        gravity = right
                            ratingBar {
                                layoutParams = LinearLayout.LayoutParams(wrapContent, wrapContent)
                                stepSize = 0.5F
                            }
                        }
                        textView {
                            id = tvRateId
                            layoutParams = LinearLayout.LayoutParams(wrapContent, wrapContent)
                            textColor = Color.BLACK

                        }


                    }


                }
            }
        }


    fun applyTemplateViewStyles(view: View) {
        when(view) {
            is RelativeLayout -> {
                view.layoutParams.height = matchParent
                view.layoutParams.width = matchParent
                when(view.layoutParams) {
                    is LinearLayout.LayoutParams -> {
                        (view.layoutParams as LinearLayout.LayoutParams).margin = view.dip(16)
                    }
                }
            }
        }
    }

}