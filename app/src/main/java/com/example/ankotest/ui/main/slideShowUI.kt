package com.example.ankotest.ui.main

import android.annotation.TargetApi
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.view.marginBottom
import com.example.ankotest.R
import org.jetbrains.anko.*
import org.jetbrains.anko.custom.style
import java.time.format.TextStyle

class slideShowUI : AnkoComponent<ViewGroup> {

    companion object {
        val tvTitleId = 1
        val tvDescId = 2
        val tvTimeId = 3
        val tvRateId = 4
        val imgThemp = 5
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
        verticalLayout {

            background = resources.getDrawable(R.drawable.shadow_box)
          lparams {
                leftMargin = 25
                width = matchParent
                height = 600
            }
                imageView {
                    id = imgThemp
                    layoutParams = LinearLayout.LayoutParams(matchParent, 300)
                    setImageDrawable(resources.getDrawable(R.drawable.not_loaded))
                    scaleType = ImageView.ScaleType.CENTER_CROP


                }

                verticalLayout {
                    lparams(matchParent, wrapContent)
                    padding = dip(10)
                    textView {
                        id = tvTitleId
                        layoutParams = LinearLayout.LayoutParams(matchParent, wrapContent)
                        text = "Sherlock"
                        textSize = 16f // <- it is sp, no worries
                        textColor = Color.BLUE
                        typeface = Typeface.DEFAULT_BOLD

                    }
                    textView {
                        id = tvDescId
                        layoutParams = LinearLayout.LayoutParams(matchParent, wrapContent)
                        text = "2009"
                        textSize = 14f
                        textColor = Color.GRAY

                    }

                    linearLayout {

                        textView {
                            id = tvTimeId
                            layoutParams = LinearLayout.LayoutParams(wrapContent, wrapContent)
                            gravity = bottom
                            textColor = Color.BLACK

                        }

                        linearLayout() {
                            gravity = left

                            ratingBar {
                                layoutParams = LinearLayout.LayoutParams(wrapContent, wrapContent)
                            }

                            textView {
                                id = tvRateId
                                layoutParams = LinearLayout.LayoutParams(0, wrapContent)
                                textColor = Color.BLACK
                                gravity = right

                            }

                        }

                    }
                }
            }
        }
    }
