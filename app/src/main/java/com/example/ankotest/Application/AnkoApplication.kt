package com.example.ankotest.Application


import com.example.ankotest.di.component.AppComponent
import com.example.ankotest.di.component.DaggerAppComponent
import com.example.ankotest.di.module.AppModule
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import timber.log.Timber

class AnkoApplication : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        var component : AppComponent = DaggerAppComponent.builder()
            .application(this)
            .appModule(AppModule(this))
            .build()
        component.inject(this)
        return component
    }

    override fun onCreate() {
        super.onCreate()
        initBaseSetting()
        injectDependencies()
        Timber.plant(Timber.DebugTree())
    }

    fun injectDependencies() {
        DaggerAppComponent.builder()
            .application(this)
            .appModule(AppModule(this))
            .build()
            .inject(this)
    }

    fun initBaseSetting() {
        /*todo add base init not for test*/
    }
}