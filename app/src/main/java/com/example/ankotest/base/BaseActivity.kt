package com.afra.reformpowermeter.Base

/**
 * @author mastermj94@gmail.com (mohammad jabbari)
 * base activity handle main methods for all activities
 * */

import android.os.Bundle
import androidx.annotation.LayoutRes

import dagger.android.support.DaggerAppCompatActivity
import timber.log.Timber
import java.lang.Exception


abstract class BaseActivity : DaggerAppCompatActivity() {




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
        } catch (e: Exception) {
            Timber.e(e, "location.onCreate.onBind problem is : ")
        }
    }

}