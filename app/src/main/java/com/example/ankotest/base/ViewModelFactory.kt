package com.afra.reformpowermeter.Base

/**
 * @author mastermj94@gmail.com (mohammad jabbari)
 * di use this factory to create view model
 * */
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Provider
import javax.inject.Singleton
import javax.inject.Inject



@Singleton
class ViewModelFactory @Inject constructor(creators: Map<Class<out ViewModel>, Provider<ViewModel>>) : ViewModelProvider.Factory
{


    private var creators: Map<Class<out ViewModel>, Provider<ViewModel>>? = creators

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        var creator: Provider<out ViewModel>? = creators?.get(modelClass)
        if (creator == null) {
            for (entry in creators?.entries!!) {
                if (modelClass.isAssignableFrom(entry.key)) {
                    creator = entry.value
                    break
                }
            }
        }
        if (creator == null) {
            throw IllegalArgumentException("unknown model class $modelClass")
        }
        try {
            return creator.get() as T
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }
}