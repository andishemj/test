package com.afra.reformpowermeter.Base

/**
 * @author mastermj94@gmail.com (mohammad jabbari)
 * this activity is view of map
 * */

import androidx.lifecycle.ViewModel

open abstract class BaseViewModel : ViewModel()
{


    override fun onCleared() {
        super.onCleared()
    }
}